#include <iostream>
#include <string>
#include <stdlib.h>
#include <vector>
#include <iomanip>

#include "checktaxid.h"

using namespace std;

int pruefziffer (vector <int>& in_Vector, int &arrayLength)
{
    int Produkt = 10;
    int Sum = 0;
    int ModSum = 0;
    int p = 0;

    for(int i = 0; i < arrayLength; i++) {

        Sum = (in_Vector[i] + Produkt);
        ModSum = Sum % 10;
        if(ModSum == 0){ModSum = 10;}
        Produkt = (2*ModSum) % 11;
        cout << "Ziffer: " << setw(4) << in_Vector[i] << " | Sum: " << setw(4) << Sum << " | Summe nach Mod 10: "  << setw(4) << ModSum << " | Produkt: "  << setw(4) << Produkt << std::endl;
    }

    cout << endl << "Remaining Produkt: " << Produkt << std::endl;
    p = 11 - Produkt;
    return (p);
}

int main()
{
    string numString;
    char t;
    vector <int> vNums;

    cout << "Bitte geben Sie eine SteuerID ein: ";
    cin >> numString;

    for(int i = 0; i < numString.length() ; i++) {
        t = numString[i];
        vNums.push_back(atoi(&t));
        cout << "Ziffern der SteuerID (vNums): " << vNums[i] << std::endl;
    }

    int tmpCount = numString.length();
//    int Result = pruefziffer(vNums, tmpCount);
//    cout << endl << "Pruefziffer: " << Result << endl;

    CheckTaxID *m_CheckTaxID = new CheckTaxID();
    m_CheckTaxID->check(vNums,tmpCount);
    int Result = m_CheckTaxID->GetCheckSum();
    int RestProdukt = m_CheckTaxID->GetProdukt();
    std::cout << "Restprodukt: " << RestProdukt <<  " | Pruefziffer (11-Restprodukt): " << Result  << endl;

    if(Result == vNums[numString.length()-1]){
        std::cout << "Alles ok .. ";
    }
    else{
        std::cout << "Nicht ok .. ";
    }

    cin >> numString;
    return 0;
}
