#ifndef CHECKTAXID_H
#define CHECKTAXID_H

#include <vector>

class CheckTaxID
{
    private:
        unsigned int m_Produkt;
        unsigned int m_Sum;
        unsigned int m_ModSum;
        unsigned int m_CheckSum;

    public:
        /* Default constructor */
        CheckTaxID() : m_Produkt (10), m_Sum(0), m_ModSum(0), m_CheckSum(0) {}  // C++11 Standard der Initialisierung, daf�r entf�llt die Definition mit separatem Konstruktor
        /* Default destructor */
        ~CheckTaxID();

        unsigned int    GetCheckSum()                   { return m_CheckSum; }
        void            SetCheckSum(unsigned int val)   { m_CheckSum = val; }
        unsigned int    GetProdukt()                    { return m_Produkt; }

        void check(std::vector <int>& in_Vector, int &in_arrayLength);

    protected:


};

#endif // CHECKTAXID_H
